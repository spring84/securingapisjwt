# Securing APIs with Spring Security and JWT

Test for securing APIs with Spring Security

The application uses Postgres SQL as database management system. As you will see in the application.yaml configuration file, the database name is: test_db.

The application is configured to generate a new "_user" table on each restart. It is this table that is stored the information of registered and authenticated users.

The respective APIs for registration and authentication are as follows:

- localhost:8001/api/auth/register
- localhost:8001/api/auth/authenticate

The secret key that are used in this application are generated from this [website](https://www.allkeysgenerator.com/)

To run the unit tests, you will first need to create a token with the following payload:

![Payload](https://user-images.githubusercontent.com/1300982/233763978-e3e58ea0-47d5-45e4-aa64-e0b2c886dcf9.png)

You can use the API to generate the token: localhost:8001/api/auth/register

Then copy the generated token and paste it at the location "module.testToken" key of the application.yaml file.

Resources:
- https://springdoc.org/v2/
- https://www.allkeysgenerator.com/
- https://www.youtube.com/watch?v=VYvqF-J2JFc
- https://www.youtube.com/watch?v=BVdQ3iuovg0