package com.visiontotale.SecuringAPIsJWT.repository;

import com.visiontotale.SecuringAPIsJWT.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Interface for user sql queries.
 */
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);
}
