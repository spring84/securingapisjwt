package com.visiontotale.SecuringAPIsJWT.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Role {
    USER("User"),
    ADMIN("Admin");

    private String label;
}
