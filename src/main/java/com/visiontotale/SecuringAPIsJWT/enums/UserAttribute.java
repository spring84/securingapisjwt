package com.visiontotale.SecuringAPIsJWT.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/** Attributes that will be used to store the claims in a token */
@Getter
@AllArgsConstructor
public enum UserAttribute {
  FIRST_NAME("firstName"),
  LAST_NAME("lastName"),
  EMAIL("sub"),
  FULL_NAME("full name"),
  ROLES("roles");

  private String label;
}
