package com.visiontotale.SecuringAPIsJWT;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(
		info = @Info(
				title = "SecuringAPIsJWT",
				version = "1.0",
				description = "Securing your APIs with Spring Security and JWT"))
public class SecuringApIsJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecuringApIsJwtApplication.class, args);
	}
}
