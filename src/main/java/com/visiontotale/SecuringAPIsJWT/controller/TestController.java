package com.visiontotale.SecuringAPIsJWT.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Test Controller. Its purpose is to check if a user can access a resource
 * to which he does not have the right to access.
 *
 * To be able to access this end point, the user must be authenticated
 * with a valid jwt token.
 */

@RestController
@RequestMapping("/api/test")
@Tag(name = "Test Ressource")
public class TestController {

    @GetMapping
    @Operation(summary = "Testing resource")
    public ResponseEntity<String> justSayHello() {
       return ResponseEntity.ok("This is a testing resource");
    }
}
