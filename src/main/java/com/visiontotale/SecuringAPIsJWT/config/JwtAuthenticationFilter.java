package com.visiontotale.SecuringAPIsJWT.config;

import com.visiontotale.SecuringAPIsJWT.service.jwt.JwtService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

/**
 * Authentication Filter, which will be the first thing that intercepts all http requests.
 * We want it to be fired each time a request comes in. That's the reason why we choose
 * to extend OncePerRequestFilter.
 */

@Component
@RequiredArgsConstructor
@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtService jwtService;
    private final UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException {

        final String authorizationHeader = request.getHeader("Authorization");
        final String jwt;
        final String userEmail;

        // Internal check to verify if there is a jwt token.
        // If the token is missing, a 403 response is returned to the client.
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            log.debug("The token is missing");
            filterChain.doFilter(request, response);
            return;
        }

        // Extracting the jwt token from the authorization header.
        jwt = authorizationHeader.substring(7);

        // Extracting the email from the jwt token.
        userEmail = jwtService.extractUsername(jwt);

        if (userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            // The user is not yet authenticated, we need the get the user from the database
            UserDetails userDetails = userDetailsService.loadUserByUsername(userEmail);
            // Check if the jwt token is still valid
            if (jwtService.isTokenValid(jwt, userDetails)) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities());

                authToken.setDetails(new WebAuthenticationDetailsSource()
                        .buildDetails(request));

                // Update the security context holder
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }

        // Pass the hand to the next filter to be executed.
        filterChain.doFilter(request, response);
    }
}
