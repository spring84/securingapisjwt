package com.visiontotale.SecuringAPIsJWT.config;

import com.visiontotale.SecuringAPIsJWT.repository.UserRepository;
import lombok.RequiredArgsConstructor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *  Class that will hold all the application configurations such as beans.
 *
 */

@Configuration
@RequiredArgsConstructor
public class AppConfig {

    private final UserRepository userRepository;
    @Bean
    public UserDetailsService userDetailsService() {
        return username -> userRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    /**
     *  This AuthenticationProvider is the data access object which is
     *  responsible to fetch the user details and also the encoded password, ...
     *
     *  It has many implementations. One of them is the DaoAuthenticationProvider
     *  which we are going to be using for this app.
     *
     * @return the authentication provider.
     */
    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();

        // Because there might be multiple implementations of the user details service,
        // we need to specify which one to be used in order to fetch information about the user.
        authProvider.setUserDetailsService(userDetailsService());

        // Then we need to provide the password encoder we are using for our app.
        authProvider.setPasswordEncoder(passwordEncoder());

        return authProvider;
    }

    /**
     *  The AuthenticationManager is the bean responsible to manage authentication.
     *
     *  @param config an object of type AuthenticationConfiguration which is injected.
     *  @return the AuthenticationManager
     */
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }

    /**
     *  Bean that returns the password encoder that we are using for the application
     *  We need to be aware of it in order to be able to decode the password using
     *  the correct algorithm.
     *
     * @return the password encoder.
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
