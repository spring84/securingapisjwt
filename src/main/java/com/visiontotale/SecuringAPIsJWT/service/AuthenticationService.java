package com.visiontotale.SecuringAPIsJWT.service;

import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.AuthenticationRequest;
import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.AuthenticationResponse;
import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.RegistrationRequest;

/**
 * Authentication service Interface.
 */
public interface AuthenticationService {
    AuthenticationResponse register(RegistrationRequest request);
    AuthenticationResponse authenticate(AuthenticationRequest request);
}
