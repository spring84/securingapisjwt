package com.visiontotale.SecuringAPIsJWT.service.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Class of the service that will be used to manipulate the jwt token.
 */

@Service
@Slf4j
public class JwtService {

    @Value("${module.secretKey}")
    private String SECRET_KEY;

    /**
     * Method that extracts the username (which in our case is the email) from the jwt token.
     *
     * @param token the jwt token.
     * @return the username
     */
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    /**
     * Method that is used to extract all the claims from the jwt token.
     *
     * @param token the jwt token.
     * @return the claims
     */
    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSigngKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * This is a generic method that is used to extract a single claim.
     *
     * @param token the jwt token.
     * @param claimsResolver function of type "Claims" and having parameter T which is the type to be returned.
     * @return the single claim.
     */
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    /**
     * Method uses the secret key to return the signing key.
     *
     * @return the signing key.
     */
    private Key getSigngKey() {
        byte[] keyByte = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyByte);
    }

    /**
     *  Method that will generate a token with claims
     *
     * @param extractClaims the claims
     * @param userDetails the user details
     * @return the generated token
     */
    public String generateToken(
            Map<String, Object> extractClaims,
            UserDetails userDetails) {

        return Jwts
                .builder()
                .setClaims(extractClaims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 30)) // Token will be valid for 30 min (It's up to you)
                .signWith(getSigngKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    /**
     *  Method that will generate a token without claims (from the user details itself)
     *
     * @param userDetails the user details.
     * @return the generated token.
     */
    public String generateToken(UserDetails userDetails) {
        return generateToken(new HashMap<>(), userDetails);
    }

    /**
     *  Method that will validate a token.
     *  Here we need the user details because along the way,
     *  we will check if the given token belongs to the user.
     *
     * @param token the token to be validated
     * @param userDetails the user details
     * @return true if the jwt token is valid, false otherwise.
     */
    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    /**
     *  Method that checks whether or not the jwt token has expired.
     *
     * @param token the jwt token to be checked.
     * @return true if the jwt token has expired, false otherwise.
     */
    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    /**
     *  Method that extracts the expiration date from the jwt token.
     *
     * @param token the jwt token to be checked.
     * @return the expiration date.
     */
    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }
}
