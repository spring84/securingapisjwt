package com.visiontotale.SecuringAPIsJWT.service.impl;

import com.visiontotale.SecuringAPIsJWT.enums.Role;
import com.visiontotale.SecuringAPIsJWT.enums.UserAttribute;
import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.AuthenticationRequest;
import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.AuthenticationResponse;
import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.RegistrationRequest;
import com.visiontotale.SecuringAPIsJWT.model.entity.User;
import com.visiontotale.SecuringAPIsJWT.repository.UserRepository;
import com.visiontotale.SecuringAPIsJWT.service.AuthenticationService;
import com.visiontotale.SecuringAPIsJWT.service.jwt.JwtService;
import java.util.Map;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/** Class that implements the Authentication service Interface. */
@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final JwtService jwtService;
  private final AuthenticationManager authenticationManager;

  @Override
  public AuthenticationResponse register(RegistrationRequest request) {

    User user =
        User.builder()
            .firstName(request.getFirstName())
            .lastName(request.getLastName())
            .email(request.getEmail())
            .password(passwordEncoder.encode(request.getPassword()))
            .roles(Set.of(Role.USER, Role.ADMIN))
            .build();

    userRepository.save(user);

    Map<String, Object> claims =
        Map.of(
            UserAttribute.FIRST_NAME.name(),
            user.getFirstName(),
            UserAttribute.LAST_NAME.name(),
            user.getLastName(),
            UserAttribute.FULL_NAME.name(),
            user.getFirstName() + " " + user.getLastName(),
            UserAttribute.ROLES.name(),
            user.getRoles());

    String jwtToken = jwtService.generateToken(claims, user);

    return AuthenticationResponse.builder().token(jwtToken).build();
  }

  @Override
  public AuthenticationResponse authenticate(AuthenticationRequest request) {
    authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));

    // Getting a user here means everything goes well.
    User user = userRepository.findByEmail(request.getEmail()).orElseThrow();

    String jwtToken = jwtService.generateToken(user);

    return AuthenticationResponse.builder().token(jwtToken).build();
  }
}
