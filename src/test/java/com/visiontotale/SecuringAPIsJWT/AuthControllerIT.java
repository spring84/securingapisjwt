package com.visiontotale.SecuringAPIsJWT;

import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.AuthenticationRequest;
import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.AuthenticationResponse;
import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.RegistrationRequest;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test of controllers for registration and authentication.
 *
 * These tests will fail because the validation with jwt token was not taken into account here.
 * The goal was just to test API calls.
 */

@SpringBootTest (webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthControllerIT extends AbstractTestIntegration {

    @LocalServerPort private int port;
    @Autowired private TestRestTemplate restTemplate;

    @Test
    void contextLoads() {
    }

    @Test
    @DisplayName("Register a new user")
    void register_new_User() {
        String url = String.format("%s:%s/%s", "http://localhost", this.port, "api/auth/register");
        restTemplate
                .getRestTemplate()
                .setInterceptors(
                        Collections.singletonList(
                                (request, body, execution) -> {
                                    request.getHeaders().put("Content-Type", List.of("application/json"));
                                    return execution.execute(request, body);
                                }));
        ResponseEntity<AuthenticationResponse> response =
                restTemplate.postForEntity(url, registrationRequest(), AuthenticationResponse.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertNotNull(response.getBody()),
                () -> assertThat(Objects.requireNonNull(response.getBody()).getToken()).isNullOrEmpty());
    }

    @Test
    @DisplayName("Authenticate a user")
    void authenticate_User() {
        String url = String.format("%s:%s/%s", "http://localhost", this.port, "api/auth/authenticate");
        restTemplate
                .getRestTemplate()
                .setInterceptors(
                        Collections.singletonList(
                                (request, body, execution) -> {
                                    request.getHeaders().put("Content-Type", List.of("application/json"));
                                    return execution.execute(request, body);
                                }));
        ResponseEntity<AuthenticationResponse> response =
                restTemplate.postForEntity(url, authenticateRequest(), AuthenticationResponse.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertNotNull(response.getBody()),
                () -> assertThat(Objects.requireNonNull(response.getBody()).getToken()).isNullOrEmpty());
    }

    @SneakyThrows
    protected RegistrationRequest registrationRequest() {
        String dataset = Files.readAllLines(
                        Paths
                                .get(new ClassPathResource("/db/dataset/register_user.json")
                                        .getURI())).stream()
                .collect(Collectors.joining(System.lineSeparator()));
        return Objects.requireNonNull(objectMapper).readValue(dataset, RegistrationRequest.class);
    }

    @SneakyThrows
    protected AuthenticationRequest authenticateRequest() {
        String dataset = Files.readAllLines(
                        Paths
                                .get(new ClassPathResource("/db/dataset/authenticate_user.json")
                                        .getURI())).stream()
                .collect(Collectors.joining(System.lineSeparator()));
        return Objects.requireNonNull(objectMapper).readValue(dataset, AuthenticationRequest.class);
    }
}
