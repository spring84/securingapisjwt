package com.visiontotale.SecuringAPIsJWT;

import com.visiontotale.SecuringAPIsJWT.enums.Role;
import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.AuthenticationRequest;
import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.AuthenticationResponse;
import com.visiontotale.SecuringAPIsJWT.model.dto.authentication.RegistrationRequest;
import com.visiontotale.SecuringAPIsJWT.model.entity.User;
import com.visiontotale.SecuringAPIsJWT.repository.UserRepository;
import com.visiontotale.SecuringAPIsJWT.service.impl.AuthenticationServiceImpl;
import com.visiontotale.SecuringAPIsJWT.service.jwt.JwtService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.Key;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class SecuringApIsJwtApplicationTests {

	@Mock
	private UserRepository userRepository;
	@Mock private AuthenticationProvider authenticationProvider;
	@Mock private AuthenticationManager authenticationManager;
	@Mock private JwtService jwtService;
	@Mock private PasswordEncoder passwordEncoder;
	@InjectMocks
	private AuthenticationServiceImpl authenticationServiceImpl;

	RegistrationRequest request;
	User user;
	AuthenticationRequest auRequest;

	@Value("${module.secretKey}")
	private String SECRET_KEY;

	@Value("${module.testToken}")
	private String token;

	@Test
	void contextLoads() {
	}

	@BeforeEach
	public void init() {
		request = RegistrationRequest.builder()
				.firstName("Jack")
				.lastName("BAUER")
				.email("bauer.jack@ctu.com")
				.password("bauer_*jack_&&")
				.build();

		auRequest = AuthenticationRequest.builder()
				.email("bauer.jack@ctu.com")
				.password("bauer_*jack_&&")
				.build();

		user = User.builder()
				.firstName(request.getFirstName())
				.lastName(request.getLastName())
				.email(request.getEmail())
				.password(request.getPassword())
				.role(Role.USER)
				.build();
	}

	@Test
	@DisplayName("Register a new user")
	void must_register_new_User() {
		// Given
		Mockito.when(userRepository.save(Mockito.any(User.class)))
				.thenReturn(user);
		Mockito.when(jwtService.generateToken(Mockito.any()))
				.thenReturn(token);

		User savedUser = userRepository.save(user);
		AuthenticationResponse response = authenticationServiceImpl.register(request);

		Jws<Claims> parsedToken = Jwts.parser()
				.setSigningKey(getSigngKey())
				.parseClaimsJws(token);

		Jws<Claims> generatedParsedToken = Jwts.parser()
				.setSigningKey(getSigngKey())
				.parseClaimsJws(response.getToken());

		//Then
		assertEquals(user.getEmail(), savedUser.getEmail());
		assertEquals(
				parsedToken.getBody().getSubject(),
				generatedParsedToken.getBody().getSubject());
	}

	@Test
	@DisplayName("Authenticate a user")
	void must_authenticate_User() {
		// Given
		Mockito.when(userRepository.findByEmail(Mockito.anyString()))
				.thenReturn(Optional.ofNullable(user));
		Mockito.when(jwtService.generateToken(Mockito.any()))
				.thenReturn(token);

		Optional<User> foundUser = userRepository.findByEmail(auRequest.getEmail());
		AuthenticationResponse response = authenticationServiceImpl.authenticate(auRequest);

		Jws<Claims> parsedToken = Jwts.parser()
				.setSigningKey(getSigngKey())
				.parseClaimsJws(token);

		Jws<Claims> generatedParsedToken = Jwts.parser()
				.setSigningKey(getSigngKey())
				.parseClaimsJws(response.getToken());

		//Then
		assertTrue(foundUser.isPresent());
		assertEquals(
				parsedToken.getBody().getSubject(),
				generatedParsedToken.getBody().getSubject());
	}

	private Key getSigngKey() {
		byte[] keyByte = Decoders.BASE64.decode(SECRET_KEY);
		return Keys.hmacShaKeyFor(keyByte);
	}

}
