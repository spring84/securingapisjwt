package com.visiontotale.SecuringAPIsJWT;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.lang.NonNull;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

/**
 * Main class for integration testing.
 *
 */

@SpringBootTest()
@AutoConfigureMockMvc
public abstract class AbstractTestIntegration {

    @Autowired
    protected ObjectMapper objectMapper;

    @SneakyThrows
    protected <T> T deserialiserObjet(@NonNull MvcResult result, Class<T> classe) {
        return objectMapper.readValue(result.getResponse().getContentAsByteArray(), classe);
    }

    @SneakyThrows
    protected <T> List<T> deserialiserListeObjet(@NonNull MvcResult result, Class<T> classe) {
        return objectMapper.readValue(
                result.getResponse().getContentAsByteArray(),
                objectMapper.getTypeFactory().constructCollectionType(List.class, classe));
    }

    @SneakyThrows
    protected <T> byte[] serialiserObjet(T contenu) {
        return objectMapper.writeValueAsBytes(contenu);
    }
}
